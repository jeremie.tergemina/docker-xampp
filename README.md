# Docker Xampp

## Prérequis

Assurez-vous d'avoir installé Docker et Docker Compose sur votre machine.

## Utilisation

1. Clonez ce dépôt sur votre machine :

   ```sh
   git clone https://gitlab.com/jeremie.tergemina/docker-xampp.git
   ```

2. Build le projet sur votre machine :

    ```sh
    docker compose up --build
    ```

3. Tout votre environnement de developpement pourra être ajouter dans le dossier development.

Essayez d'ajouter un index.php avec ce que vous voulez dedans.

4. Testez que tous les services fonctionnent correctement. Les URLs à tester sont :

Si vous ajouter un index.php directement a la racine de development vous n'aurez qu'a mettre "http://localhost/", sinon :

http://localhost/nomdevotrefichier : devrait afficher la page

http://adminer.localhost/ : devrait afficher l'interface d'administration de MySQL

5. Pour arreter les services : 

    ```sh
    docker compose up --build
    ```
## Contribuer

Si vous souhaitez contribuer à ce projet, n'hésitez pas à ouvrir une issue ou une pull request ! 